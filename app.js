import {sumar, restar, multiplicar, dividir} from './calculator.js';
import fs from 'fs';

let a = 5;
let b = 7;

let resultado = '';

resultado += "Suma: " + sumar(a, b) + "\n";
resultado += "Restar: " + restar(a, b) + "\n";
resultado += "Multiplicar: " + multiplicar(a, b) + "\n";
resultado += "Dividir: " + dividir(a, b) + "\n";
resultado += "---------------------------------------------"  + "\n";

console.log(resultado);

fs.appendFileSync("resultados.log", resultado);

